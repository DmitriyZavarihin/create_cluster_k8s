ip_master=$(grep -A 1 master ~/.ssh/config | grep HostName | sed 's/[^0-9.]//g')
ip_worker1=$(grep -A 1 worker1 ~/.ssh/config | grep HostName | sed 's/[^0-9.]//g')
ip_worker2=$(grep -A 1 worker2 ~/.ssh/config | grep HostName | sed 's/[^0-9.]//g')

sed -i 's/ip_master/ip_master=$(ip_master)' /Users/dmitrii/ansible/hosts
#sed -i 's/^ip_worker1/ip_worker1=$ip_master' ./hosts
#sed -i 's/^ip_worker2/ip_worker2=$ip_master' ./hosts
